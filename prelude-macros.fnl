;; prelude-macros.fnl, contains all the macros by this prelude.

;; body1 is needed so we can verify there is at least one body of code.
(fn dotimes [n body1 ...]
  (assert body1 "expected body")
  (assert n "expected times to repeat body")
  ;; extra check because lua for _ = 0, 0 is broken.
  `(when (> @n 0)
     (for [_ 1 @n]
       @body1 @...)))

{ :dotimes dotimes }
