;; prelude.fnl, everything except the macros

;; Allow for multiplication on string values.
;; for example: (* "foo" 3) => "foofoofoo"
(tset (getmetatable "") :__mul string.rep)
