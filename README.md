# fennel-prelude
a prelude of functions to use with the fennel programming language (fennel-lang.org), 

i really like fennel but i think it lacks some ease of use functions that it should really have.

# Usage
```fennel
(require-macros :prelude-macros)
(require        :prelude-functions)
```


# Documentation
TODO
